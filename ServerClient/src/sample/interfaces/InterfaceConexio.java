package sample.interfaces;

public interface InterfaceConexio {
    public void open();

    public void close();
}