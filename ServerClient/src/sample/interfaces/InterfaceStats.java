package sample.interfaces;

import sample.clases.Stat;

public interface InterfaceStats extends Interface<Stat> {
    public Stat getByUserId(int userId);

    public void updateStat(Stat stat, int userId);
}
