package sample.interfaces;

import sample.clases.User;

public interface InterfaceUser extends Interface<User> {

    public User getByUsername(String name);
}
