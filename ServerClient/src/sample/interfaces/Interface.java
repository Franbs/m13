package sample.interfaces;

import sample.clases.User;

import java.util.List;

public interface Interface<T> {

    public boolean insert(T user);

    public List<T> getAll();

    public int getBiggerId();
}