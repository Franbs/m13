package sample.utils;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Controllers.PawnController;
import sample.clases.Coordinate;
import sample.clases.Piece;

import javax.swing.text.Position;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author David Ortega
 * @author Francesc Banzo
 */

public class ChessUtils {
    /**
     * @param piece
     * @param gridPane
     * @param pos
     * Mueve las piezas eliminadas a los lados del tablero
     */
    public static void movePieceToBench(Piece piece, GridPane gridPane, Coordinate pos) {
        if (piece.getColorPiece().equals("white")) {
            ImageView image = (ImageView) getNodeByRowColumn(pos.getRow(), pos.getColumn(), gridPane);

            image.setVisible(true);

            pos.setColumn(pos.getColumn() - 1);
        } else {
            ImageView image = (ImageView) getNodeByRowColumn(pos.getRow(), pos.getColumn(), gridPane);

            image.setVisible(true);

            pos.setColumn(pos.getColumn() + 1);
        }
    }

    /**
     * @param positions
     * @param board
     * Ilumina las posiciones validas de la pieza seleccionada
     */
    public static void recolorPane(ArrayList<Coordinate> positions, GridPane board) {
        for (Coordinate pos : positions) {
            AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
            if (pos.getRow() % 2 == 0) {
                if (pos.getColumn() % 2 == 0) {
                    pane.setStyle("-fx-background-color :  #ebebd0");
                } else {
                    pane.setStyle("-fx-background-color :  #88001b");
                }
            } else {
                if (pos.getColumn() % 2 == 0) {
                    pane.setStyle("-fx-background-color :  #88001b");
                } else {
                    pane.setStyle("-fx-background-color :  #ebebd0");
                }
            }
        }
    }

    /**
     * @param piece
     * @param rowI
     * @param columnI
     * @param positions
     * @param board
     * @param pieces
     * Comprueba las posiciones validas de la pieza i las guarda en una lista
     */
    public static void checkMovements(Piece piece, int rowI, int columnI, ArrayList<Coordinate> positions, GridPane board, ArrayList<Piece> pieces) {
        int row = rowI;
        int column = columnI;

        switch (piece.getPieceType()) {
            case "pawn":
                if (piece.getColorPiece().equals("white")) {
                    // Si es el primer movimiento del peon
                    if (rowI == 6) {
                        // Bucle para comprobar las dos casillas en frente del peon, cuando esta en la 1a posicion
                        for (int i = 1; i < 3; i++) {
                            if (getImageId(row - i, column, board) == null) {
                                Coordinate position = new Coordinate(row - i, column);
                                positions.add(position);
                            } else {
                                break;
                            }
                        }
                    } else {
                        // Posicion de delante
                        row--;
                        if (getImageId(row, column, board) == null) {
                            Coordinate position = new Coordinate(row, column);
                            positions.add(position);
                        }
                    }

                    // Diagonal derecha
                    row = rowI;
                    row--;
                    column++;

                    // Si hay una pieza en la diagonal
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        // Si la pieza es negra la posicion es valida
                        if (pieceToEat.getColorPiece().equals("black")) {
                            Coordinate position = new Coordinate(row, column);
                            positions.add(position);
                        }
                    }

                    // Diagonal izquierda
                    column = columnI;
                    column--;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id,pieces);

                        if (pieceToEat.getColorPiece().equals("black")){
                            Coordinate position = new Coordinate(row, column);
                            positions.add(position);
                        }
                    }
                    // Si la pieza es negra
                } else {
                    // Si es el primer movimiento del peon
                    if (rowI == 1) {
                        // Bucle para comprobar las dos casillas en frente del peon, cuando esta en la 1a posicion
                        for (int i = 1; i < 3; i++) {
                            if (getImageId(row + i, column, board) == null) {
                                Coordinate position = new Coordinate(row + i, column);
                                positions.add(position);
                            } else {
                                break;
                            }
                        }
                    } else{
                        // Posicion de delante
                        row++;
                        if (getImageId(row, column, board) == null) {
                            Coordinate position = new Coordinate(row, column);
                            positions.add(position);
                        }
                    }

                    // Diagonal derecha
                    row = rowI;
                    row++;
                    column++;
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (pieceToEat.getColorPiece().equals("white")) {
                            Coordinate position = new Coordinate(row, column);
                            positions.add(position);
                        }
                    }

                    // Diagonal izquierda
                    column = columnI;
                    column--;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (pieceToEat.getColorPiece().equals("white")){
                            Coordinate position = new Coordinate(row, column);
                            positions.add(position);
                        }
                    }
                }

                break;

            case "queen":
                // Movimiento hacia arriba
                for (int i = row - 1; i > -1; i--) {
                    if (getImageId(i, column, board) != null) {
                        int id = Integer.parseInt(getImageId(i, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        // Si la reina es blanca
                        if (piece.getColorPiece().equals("white")) {
                            // Comprobamos si la pieza en la trayectoria es negra (es decir, se puede comer)
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                            // Si la reina es negra
                        } else {
                            // Comprobamos si la pieza en la trayectoria es blanca (es decir, se puede comer)
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(i, column);
                        positions.add(position);
                    }
                }

                // Movimiento hacia abajo
                for (int i = row + 1; i < 8; i++) {
                    if (getImageId(i, column, board) != null) {
                        int id = Integer.parseInt(getImageId(i, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(i, column);
                        positions.add(position);
                    }
                }

                // Movimiento hacia la izquierda
                for (int i = column - 1; i >= 0; i--) {
                    if (getImageId(row, i, board) != null) {
                        int id = Integer.parseInt(getImageId(row, i, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, i);
                        positions.add(position);
                    }
                }

                // Movimiento hacia la deracha
                for (int i = column + 1; i < 8; i++) {
                    if (getImageId(row, i, board) != null) {
                        int id = Integer.parseInt(getImageId(row, i, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, i);
                        positions.add(position);
                    }
                }

                // Diagonal arriba derecha
                boolean exit = false;
                row = rowI;
                column = columnI;

                if (row <= 0 || column >= 7) {
                    exit = true;
                }

                while (exit == false) {
                    row--;
                    column++;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row <= 0 || column >= 7) {
                        exit = true;
                    }
                }

                // Diagonal arriba izquierda
                exit = false;
                row = rowI;
                column = columnI;

                if (row <= 0 || column <= 0) {
                    exit = true;
                }

                while (exit == false) {
                    row--;
                    column--;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row <= 0 || column <= 0) {
                        exit = true;
                    }
                }

                // Diagonal abajo izquierda
                exit = false;
                row = rowI;
                column = columnI;

                if (row >= 7 || column <= 0) {
                    exit = true;
                }

                while (exit == false) {
                    row++;
                    column--;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row >= 7 || column <= 0) {
                        exit = true;
                    }
                }

                // Diagonal abajo derecha
                exit = false;
                row = rowI;
                column = columnI;

                if (row >= 7 || column >= 7) {
                    exit = true;
                }

                while (exit == false) {
                    row++;
                    column++;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit = true;
                            } else {
                                exit = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row >= 7 || column >= 7) {
                        exit = true;
                    }
                }
                break;

            case "king":
                // Arriba
                row++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Abajo
                row = rowI;
                row--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Izquierda
                row = rowI;
                column--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Derecha
                column = columnI;
                column++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Arriba derecha(diagonal)
                column = columnI;
                row = rowI;
                row++;
                column++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Arriba izquierda(diagonal)
                column = columnI;
                row = rowI;
                row++;
                column--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Abajo izquierda(diagonal)
                column = columnI;
                row = rowI;
                row--;
                column--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Abajo derecha(diagonal)
                column = columnI;
                row = rowI;
                row--;
                column++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }
                /*} else {

                }*/
                break;

            case "rook":
                // Movimiento hacia arriba
                for (int i = row - 1; i > -1; i--) {
                    if (getImageId(i, column, board) != null) {
                        int id = Integer.parseInt(getImageId(i, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(i, column);
                        positions.add(position);
                    }
                }

                // Movimiento hacia abajo
                for (int i = row + 1; i < 8; i++) {
                    if (getImageId(i, column, board) != null) {
                        int id = Integer.parseInt(getImageId(i, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(i, column);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(i, column);
                        positions.add(position);
                    }
                }

                // Movimiento hacia la izquierda
                for (int i = column - 1; i >= 0; i--) {
                    if (getImageId(row, i, board) != null) {
                        int id = Integer.parseInt(getImageId(row, i, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, i);
                        positions.add(position);
                    }
                }

                // Movimiento hacia la deracha
                for (int i = column + 1; i < 8; i++) {
                    if (getImageId(row, i, board) != null) {
                        int id = Integer.parseInt(getImageId(row, i, board));
                        Piece pieceToEat = getPieceById(id, pieces);

                        if (piece.getColorPiece().equals("white")) {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        } else {
                            //Comprobamos si la pieza es negra
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, i);
                                positions.add(position);
                                break;
                            } else {
                                break;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, i);
                        positions.add(position);
                    }
                }
                /*} else {

                }*/
                break;

            case "bishop":
                // Diagonal arriba derecha
                boolean exit2 = false;

                if (row <= 0 || column >= 7) {
                    exit2 = true;
                }

                while (exit2 == false) {
                    row--;
                    column++;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row <= 0 || column >= 7) {
                        exit2 = true;
                    }
                }

                // Diagonal arriba izquierda
                exit2 = false;
                row = rowI;
                column = columnI;

                if (row <= 0 || column <= 0) {
                    exit2 = true;
                }

                while (exit2 == false) {
                    row--;
                    column--;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row <= 0 || column <= 0) {
                        exit2 = true;
                    }
                }

                // Diagonal abajo izquierda
                exit2 = false;
                row = rowI;
                column = columnI;

                if (row >= 7 || column <= 0) {
                    exit2 = true;
                }

                while (exit2 == false) {
                    row++;
                    column--;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row >= 7 || column <= 0) {
                        exit2 = true;
                    }
                }

                // Diagonal abajo derecha
                exit2 = false;
                row = rowI;
                column = columnI;

                if (row >= 7 || column >= 7) {
                    exit2 = true;
                }

                while (exit2 == false) {
                    row++;
                    column++;

                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                                exit2 = true;
                            } else {
                                exit2 = true;
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }

                    if (row >= 7 || column >= 7) {
                        exit2 = true;
                    }
                }
                /*} else {

                }*/
                break;

            case "knight":
                // Para arriba derecha
                row += 2;
                column++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para arriba izquierda
                column = columnI;
                column--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para la derecha arriba
                column = columnI;
                row = rowI;
                column += 2;
                row++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para la derecha abajo
                column = columnI;
                row = rowI;
                column += 2;
                row--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para la izquierda abajo
                column = columnI;
                row = rowI;
                column -= 2;
                row--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para la izquierda arriba
                column = columnI;
                row = rowI;
                column -= 2;
                row++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para abajo izquierda
                row = rowI;
                column = columnI;
                row -= 2;
                column--;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }

                // Para abajo derecha
                row = rowI;
                column = columnI;
                row -= 2;
                column++;
                if (row >= 0 && row <= 7 && column >= 0 && column <= 7) {
                    if (getImageId(row, column, board) != null) {
                        int id = Integer.parseInt(getImageId(row, column, board));
                        Piece pieceToEat = getPieceById(id, pieces);
                        if (piece.getColorPiece().equals("white")) {
                            if (pieceToEat.getColorPiece().equals("black")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        } else {
                            if (pieceToEat.getColorPiece().equals("white")) {
                                Coordinate position = new Coordinate(row, column);
                                positions.add(position);
                            }
                        }
                    } else {
                        Coordinate position = new Coordinate(row, column);
                        positions.add(position);
                    }
                }
                /*} else {

                }*/
                break;
        }
    }

    /**
     * @param row
     * @param column
     * @param board
     * @return el nodo que hay en la posicion especificada del gridPane
     */
    public static Node getNodeByRowColumn(final int row, final int column, GridPane board) {
        Node result = null;
        ObservableList<Node> childrens = board.getChildren();

        // Recorremos la lista de nodos del tablero
        for (Node node : childrens) {
            if(board.getRowIndex(node) == row && board.getColumnIndex(node) == column) {
                result = node;
                break;
            }
        }

        return result;
    }

    /**
     *
     * @param id
     * @param pieces
     * @return la pieza que tenga el mismo id que el que le pasamos por parametro
     */
    public static Piece getPieceById (int id, ArrayList<Piece> pieces) {
        Piece piece = null;

        // Recorremos la lista de todas las piezas del tablero
        for (Piece tmpPiece : pieces) {
            if (tmpPiece.getId() == id){
                piece = tmpPiece;
                break;
            }
        }

        return piece;
    }

    /**
     * @param row
     * @param column
     * @param board
     * @return la id de la pieza
     * Si encuentra una pieza devuelve la id, si no devuelve null
     */
    public static String getImageId (int row, int column, GridPane board) {
        String id = null;
        ObservableList<Node> childrens = board.getChildren();
        for (Node node : childrens) {
            if (row == board.getRowIndex(node) && column == board.getColumnIndex(node)) {
                String tmpId = node.getId();
                if (tmpId != null) {
                    id = tmpId;
                    break;
                }
            }
        }

        return id;
    }

    // Funcion para sokets (no se usa)
    /*public static int convertColumns (String column) {
        int base = (int) 'a';

        char entrada = column.charAt(0);
        int intColumnF = Math.abs(base - (int) entrada);

        return intColumnF;
    }

    public static int convertRows (int row) {
        int rowF = 8 - row;

        return rowF;
    }*/
}