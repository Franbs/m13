package sample.clases;

import javafx.scene.image.ImageView;

public class Piece {
    int id;
    String pieceType;
    String colorPiece;
    ImageView image;
    int row;
    int column;

    public Piece() {
    }

    public Piece(int id, String pieceType, String colorPiece, ImageView image, int row, int column) {
        this.id = id;
        this.pieceType = pieceType;
        this.colorPiece = colorPiece;
        this.image = image;
        this.row = row;
        this.column = column;
    }

    /*public Piece(int id, String pieceType, String colorPiece, ImageView image) {
        this.id = id;
        this.pieceType = pieceType;
        this.colorPiece = colorPiece;
        this.image = image;
    }*/

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPieceType() {
        return pieceType;
    }

    public void setPieceType(String pieceType) {
        this.pieceType = pieceType;
    }

    public String getColorPiece() {
        return colorPiece;
    }

    public void setColorPiece(String colorPiece) {
        this.colorPiece = colorPiece;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Piece{" +
                "pieceType='" + pieceType + '\'' +
                ", colorPiece='" + colorPiece + '\'' +
                ", id=" + id +
                '}';
    }
}