package sample.clases;

public class Stat {
    int id;
    int userId;
    int gamesPlayed;
    int gamesWon;
    int gamesLost;
    double winRate;
    int winStreak;

    public Stat() {
    }

    public Stat(int id, int userId, int gamesPlayed, int gamesWon, int gamesLost, double winRate, int winStreak) {
        this.id = id;
        this.userId = userId;
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
        this.winRate = winRate;
        this.winStreak = winStreak;
    }

    @Override
    public String toString() {
        return "Stat{" +
                "id=" + id +
                ", userId=" + userId +
                ", gamesPlayed=" + gamesPlayed +
                ", gamesWon=" + gamesWon +
                ", gamesLost=" + gamesLost +
                ", winRate=" + winRate +
                ", winStreak=" + winStreak +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getGamesLost() {
        return gamesLost;
    }

    public void setGamesLost(int gamesLost) {
        this.gamesLost = gamesLost;
    }

    public double getWinRate() {
        return winRate;
    }

    public void setWinRate(double winRate) {
        this.winRate = winRate;
    }

    public int getWinStreak() {
        return winStreak;
    }

    public void setWinStreak(int winStreak) {
        this.winStreak = winStreak;
    }
}
