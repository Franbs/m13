package sample.DAO;

import sample.interfaces.InterfaceConexio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class DAOConexioJDBC implements InterfaceConexio {

    Connection con;
    private String url;
    private String usuari;
    private String password;

    public DAOConexioJDBC() {
    }

    /**
     * Se abre la conexion a la db
     */
    @Override
    public void open() {
        try {
            url = "jdbc:mysql://localhost/db2?serverTimezone=UTC"; // db, 3306
            usuari = "Fran";
            password = "";
            // Fas la connexio amb els valors que t'ha pasat l'usuari
            this.con = DriverManager.getConnection(url, usuari, password);
            System.out.println("Connexió oberta correctament");

        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("La connexió no s'ha pogut realiztar correctament");
        }
    }

    /**
     * Se cierra
     */
    @Override
    public void close() {
        try {
            // Tanquem la connexio
            con.close();
            System.out.println("Connexió tancada correctament");

        } catch (SQLException ex) {
            System.err.println("Error al tancar la connexió");
        }
    }

    // Getters i setters
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsuari() {
        return usuari;
    }

    public void setUsuari(String usuari) {
        this.usuari = usuari;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
