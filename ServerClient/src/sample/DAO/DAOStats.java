package sample.DAO;

import sample.clases.Stat;
import sample.clases.User;
import sample.interfaces.InterfaceStats;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class DAOStats implements InterfaceStats {
    /**
     * Funcion para introducir una stat
     * @param stat
     * @return boolean
     */
    @Override
    public boolean insert(Stat stat) {
        boolean insertedSuccesfully = false;

        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        String sentenciaSQL1 = "INSERT INTO stats (id, userId, gamesPlayed, gamesWon, gamesLost, winRate, winStreak) values (?,?,?,?,?,?,?)";
        PreparedStatement sentenciaPreparada1 = null;

        // Se inserta
        try {
            sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
            sentenciaPreparada1.setInt(1, stat.getUserId());
            sentenciaPreparada1.setInt(2, stat.getId());
            sentenciaPreparada1.setInt(3, stat.getGamesPlayed());
            sentenciaPreparada1.setInt(4, stat.getGamesWon());
            sentenciaPreparada1.setInt(5, stat.getGamesLost());
            sentenciaPreparada1.setDouble(6, stat.getWinRate());
            sentenciaPreparada1.setInt(7, stat.getWinStreak());
            sentenciaPreparada1.executeUpdate();
            System.out.println("Stat successfully inserted");

            insertedSuccesfully = true;

        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            //System.err.println("insert DAOUser: " + throwables);
            throwables.printStackTrace();

            String text = "insert DAOUser: \n";
            //Log.save(text, throwables);
        }

        return insertedSuccesfully;
    }

    /**
     * Funcion para coger todas las stats
     * @return ArrayList
     */
    @Override
    public List<Stat> getAll() {
        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        List<Stat> listStats = new ArrayList<Stat>();

        String sentenciaSQL = "SELECT id, userId, gamesPlayed, gamesWon, gamesLost, winRate, winStreak FROM stats";
        Statement statement = null;

        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            while (rs.next()) {
                Stat stat = new Stat();
                stat.setId(rs.getInt("id"));
                stat.setUserId(rs.getInt("userId"));
                stat.setGamesPlayed(rs.getInt("gamesPlayed"));
                stat.setGamesPlayed(rs.getInt("gamesWon"));
                stat.setGamesPlayed(rs.getInt("gamesLost"));
                stat.setWinRate(rs.getDouble("winRate"));
                stat.setWinStreak(rs.getInt("winStreak"));
                listStats.add(stat);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } finally {
            conexioJDBC.close();
        }

        return listStats;
    }

    /**
     * Funcion para coger el id más grande de las stats de la db
     * @return maxId
     */
    @Override
    public int getBiggerId() {
        int maxId = 1;

        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        String sentenciaSQL = "select * from stats where id = (select max(id) from stats)";
        Statement statement = null;

        try {
            List<Stat> llistaStats = getAll();

            if (llistaStats.size() != 0) {
                statement = conexioJDBC.con.createStatement();
                ResultSet rs = statement.executeQuery(sentenciaSQL);

                rs.next();
                maxId = rs.getInt("id");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } finally {
            conexioJDBC.close();
        }

        return maxId;
    }

    /**
     * Funcion para coger una stat por su userId
     * @param userId
     * @return Stat
     */
    @Override
    public Stat getByUserId(int userId) {
        Stat stat = new Stat();

        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        //String sentenciaSQL = "SELECT id, userId, gamesPlayed, gamesWon, gamesLost, winRate, winStreak FROM stats where userId = '" + userId + "'";
        String sentenciaSQL = "SELECT * from stats where userId = '" + userId + "'";
        Statement statement = null;

        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            while (rs.next()) {
                stat.setId(rs.getInt("id"));
                stat.setUserId(rs.getInt("userId"));
                stat.setGamesPlayed(rs.getInt("gamesPlayed"));
                stat.setGamesWon(rs.getInt("gamesWon"));
                stat.setGamesLost(rs.getInt("gamesLost"));
                stat.setWinRate(rs.getDouble("winRate"));
                stat.setWinStreak(rs.getInt("winStreak"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } finally {
            conexioJDBC.close();
        }

        return stat;
    }

    /**
     * Funcion para actualizar una stat
     * @param stat
     * @param userId
     */
    @Override
    public void updateStat(Stat stat, int userId) {
        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        // Actualitzem les dades del estudi
        String sentenciaSQL1 = "UPDATE stats SET id = ?, userId = ?, gamesPlayed = ?, gamesWon = ?, gamesLost = ?, winRate = ?, winStreak = ? where userId = ?";
        PreparedStatement sentenciaPreparada1 = null;

        try {
            sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
            sentenciaPreparada1.setInt(1, stat.getId());
            sentenciaPreparada1.setInt(2, stat.getUserId());
            sentenciaPreparada1.setInt(3, stat.getGamesPlayed());
            sentenciaPreparada1.setInt(4, stat.getGamesWon());
            sentenciaPreparada1.setInt(5, stat.getGamesLost());
            sentenciaPreparada1.setDouble(6, stat.getWinRate());
            sentenciaPreparada1.setInt(7, stat.getWinStreak());
            sentenciaPreparada1.setInt(8, userId);
            sentenciaPreparada1.executeUpdate();
            System.out.println("Stat actualitzada correctament.");

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            conexioJDBC.close();
        }
    }
}
