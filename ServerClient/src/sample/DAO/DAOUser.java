package sample.DAO;

import javafx.scene.control.Alert;
import sample.clases.Stat;
import sample.clases.User;
import sample.interfaces.InterfaceUser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class DAOUser implements InterfaceUser {
    /**
     * Funcion para insertar un user
     * @param user
     * @return boolean
     */
    @Override
    public boolean insert(User user) {
        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        // Agafem tots els estudis de la base de dades
        List<User> listUsers = getAll();
        boolean userAndEmailExsist = false;

        // Si el user no esta repetit l'insertem
        conexioJDBC.open();

        // Para encriptar la contraseña se puede hacer sha1(?) en el interrogante de password, o sea al introducirla
        String sentenciaSQL1 = "INSERT INTO users (id, email, username, password) values (?,?,?,sha(?))";
        PreparedStatement sentenciaPreparada1 = null;
        try {
            // Comprobem que no existeix cap estudi amb la id del estudi que hem d'insertar
            for (User u : listUsers) {
                if (u.getUsername().equals(user.getUsername()) || u.getEmail().equals(user.getEmail())) {
                    userAndEmailExsist = true;
                    //throw new ClauException("El id no pot ser el mateix");
                    break;
                }
            }

            // S'inserta
            if (!userAndEmailExsist) {
                sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
                sentenciaPreparada1.setInt(1, user.getId());
                sentenciaPreparada1.setString(2, user.getEmail());
                sentenciaPreparada1.setString(3, user.getUsername());
                sentenciaPreparada1.setString(4, user.getPassword());
                sentenciaPreparada1.executeUpdate();
                System.out.println("User successfully inserted");

                // Es crea una stat
                DAOStats daoStats = new DAOStats();
                Stat stat = new Stat(user.getId(), daoStats.getBiggerId(), 0, 0, 0, 0, 0);
                daoStats.insert(stat);

            } else {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                errorAlert.setHeaderText("Error");
                errorAlert.setContentText("A user with that username already exists");
                errorAlert.showAndWait();
            }

        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("insert DAOUser: " + throwables);
            throwables.printStackTrace();

            String text = "insert DAOUser: \n";
            //Log.save(text, throwables);

        } catch (Exception e) {
            System.err.println("insert DAOUser: " + e);

            String text = "insert DAOUser: \n";
            //Log.save(text, e);

        } finally {
            conexioJDBC.close();
        }

        return userAndEmailExsist;
    }

    /**
     * Funcion para coger todos los usuarios de la db
     * @return List
     */
    @Override
    public List getAll() {
        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        List<User> listUsers = new ArrayList<User>();

        // Se seleccionan todos los users
        String sentenciaSQL = "SELECT id,username,password FROM users";
        Statement statement = null;
        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            // Se meten en una lista mientras se recorren los resultados
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                listUsers.add(user);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } finally {
            conexioJDBC.close();
        }

        return listUsers;
    }

    /**
     * Funcion para coger el id más grande de la db
     * @return maxId
     */
    @Override
    public int getBiggerId() {
        int maxId = 0;

        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        // Coge el usuario con el id más grande
        String sentenciaSQL = "select * from users where id = (select max(id) from users)";
        Statement statement = null;

        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            // Se saca su id
            rs.next();
            maxId = rs.getInt("id");

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } finally {
            conexioJDBC.close();
        }

        return maxId;
    }

    /**
     * Funcion para coger el usuario con su username
     * @param name
     * @return User
     */
    @Override
    public User getByUsername(String name) {
        User user = new User();

        DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
        conexioJDBC.open();

        // Se seleccionan los users que coincidan su nombre o email con el introducido
        String sentenciaSQL = "select * from users where username = '" + name + "' or email = '" + name + "'";
        Statement statement = null;

        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();

        } finally {
            conexioJDBC.close();
        }

        return user;
    }
}