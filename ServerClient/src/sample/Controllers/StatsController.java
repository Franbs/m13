package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sample.DAO.DAOStats;
import sample.clases.Stat;

import java.io.IOException;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class StatsController {
    @FXML
    private Text stats;

    @FXML
    private Text gamesPlayed;

    @FXML
    private Text gamesWon;

    @FXML
    private Text gamesLost;

    @FXML
    private Text winRate;

    @FXML
    private Text winStreak;

    @FXML
    private Button btBack;

    /**
     * Funcion que se ejecutara cuando se inicie el fxml
     */
    public void initialize() {
        DAOStats daoStats = new DAOStats();

        // Se coge el nombre del usuario con el user static que hemos creado en el login
        stats.setText(LoginController.user.getUsername() + "'s statistics");

        // Se coge las estadisticas relacionadas con ese usuario
        Stat stat = daoStats.getByUserId(LoginController.user.getId());

        // Se ponen sus estadisticas en los campos de texto
        gamesPlayed.setText("" + stat.getGamesPlayed());
        gamesWon.setText("" + stat.getGamesWon());
        gamesLost.setText("" + stat.getGamesLost());
        winRate.setText("" + stat.getWinRate() + " %");
        winStreak.setText("" + stat.getWinStreak());
    }

    /**
     * Funcion para ir para atras
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickBack(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("play.fxml"));
        Stage stage = (Stage) btBack.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }
}
