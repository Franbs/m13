package sample.Controllers;

import javafx.animation.KeyFrame;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

/*toggl taiga

--module-path
/home/users/inf/wiam2/iam21773506/Documents/openjfx-11.0.2_linux-x64_bin-sdk/javafx-sdk-11.0.2/lib
--add-modules
javafx.controls,javafx.fxml

--module-path
"C:\Users\12fra\Documents\Escola del treball\jars\javafx-sdk-11.0.2\lib"
--add-modules
javafx.controls,javafx.fxml*/

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class Main extends Application {
    /**
     * Funcion que se ejecutara al principio de la app
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Se crea un border pane
        BorderPane root = new BorderPane();
        // Se le pone un color negro de fondo
        root.setStyle("-fx-background-color: #231f20");
        // Se pone un pane en el centro del borrder pane
        Pane pane = new Pane();
        root.setCenter(pane);

        // Se crea una imagen con el logo
        ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream("../images/logo.png")));

        // Se ejecuta una animacion
        animation(imageView);

        // Se añade la imagen a la ventana
        root.getChildren().addAll(imageView);

        //Se le pone un titulo, icono, tamaño al panel y se muestra
        primaryStage.setTitle("ChessFD");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("../images/logo.png")));
        primaryStage.setScene(new Scene(root, 750, 500));
        primaryStage.setResizable(false);
        primaryStage.show();

        // Al cabo de 2 segundos y medio se hace un intent al siguiente panel
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2.5), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    // Se hace el intent
                    intentLogin(primaryStage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }));

        // Que solo se ejecute una vez
        timeline.setCycleCount(1);
        // Que empieze el timeline
        timeline.play();
    }

    /**
     * Funcion que hara la animacion
     * @param imageView
     */
    public void animation(ImageView imageView) {
        // Tamaño inicial de la imagen
        imageView.setFitHeight(1);
        imageView.setFitWidth(1);
        // Posicion
        imageView.setLayoutX(370);
        imageView.setLayoutY(200);

        // Se crea una transicion de tamaño
        ScaleTransition scale = new ScaleTransition();
        // Tamaño
        scale.setByX(150);
        scale.setByY(150);
        scale.setNode(imageView);
        // Duracion
        scale.setDuration(Duration.millis(1000));
        scale.setDelay(Duration.millis(1000));
        scale.play();
    }

    /**
     * @param primaryStage
     * @throws IOException
     */
    public void intentLogin(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.setTitle("ChessFD");
        primaryStage.setScene(new Scene(root, 750, 500));
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
