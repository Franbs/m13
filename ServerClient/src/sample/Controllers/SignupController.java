package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DAO.DAOUser;
import sample.clases.User;
import sample.interfaces.Interface;

import java.io.IOException;
import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class SignupController {
    public static User user;

    // Botones
    @FXML
    private Button btSignUp;

    @FXML
    private Button btBack;

    // Texto
    @FXML
    private TextField email;

    @FXML
    private TextField username;

    @FXML
    private TextField password;

    /**
     * Funcion para hacer el sign up
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickSignUp(ActionEvent actionEvent) throws IOException {
        try {
            // Se crea un user
            DAOUser daoUser = new DAOUser();
            user = new User();

            // Se cogen los users de la db
            List<User> llistaUsers = daoUser.getAll();

            // Si no hay ninguno se le asigna id = 1
            if (llistaUsers.size() == 0) {
                user.setId(1);
            // Si hay alguno, se coge el id más grande y se le asigna el siguiente
            } else {
                int idNew = (daoUser.getBiggerId() + 1);
                user.setId(idNew);
            }

            user.setEmail(email.getText());

            user.setUsername(username.getText());

            // Restricciones del tamaño de la contraseña
            if (password.getText().length() < 20 && password.getText().length() > 5) {
                user.setPassword(password.getText());

                // Se inserta el usuario si no existe y sus datos son correctos
                if (!daoUser.insert(user)) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
                    Stage stage = (Stage) btSignUp.getScene().getWindow();
                    Scene scene = new Scene(loader.load());
                    stage.setScene(scene);

                    Alert errorAlert = new Alert(Alert.AlertType.INFORMATION);
                    errorAlert.setHeaderText("Notification");
                    errorAlert.setContentText("The user has been created successfully");
                    errorAlert.showAndWait();
                }
            } else {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                errorAlert.setHeaderText("Error");
                errorAlert.setContentText("The password must be between 5 and 20\ncharacters long");
                errorAlert.showAndWait();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Boton de ir para atras
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickBack(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
        Stage stage = (Stage) btBack.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }
}