package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.awt.*;
import java.io.IOException;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class PawnController {
    public static boolean knight = false;

    @FXML
    private javafx.scene.control.Button knightButton;

    @FXML
    private javafx.scene.control.Button queenButton;

    /**
     * Funcion al pulsar la reina
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickQueen(ActionEvent actionEvent) throws IOException {
        knight = false;
        // Cogemos el stage
        Stage stage = (Stage) queenButton.getScene().getWindow();
        // Lo cerramos
        stage.close();

    }

    /**
     * Funcion al pulsar el caballo
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickKinght(ActionEvent actionEvent) throws IOException {
        knight = true;
        // Cogemos el stage
        Stage stage = (Stage) knightButton.getScene().getWindow();
        // Lo cerramos
        stage.close();
    }
}
