package sample.Controllers;

import javafx.scene.effect.Bloom;
import javafx.scene.effect.Glow;
import javafx.stage.Modality;
import javafx.stage.Window;
import sample.DAO.DAOStats;
import sample.clases.Stat;
import sample.utils.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sample.clases.Piece;
import sample.clases.Coordinate;

import javax.swing.text.Position;
import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class ChessController {
    /*
    #ebebd0
    #88001b

    White
    1 - 8 pawn
    9 - 10 rook
    11 - 12 knight
    13 - 14 bishop
    15 queen
    16 king

    Black
    17 - 24 pawn
    25 - 26 rook
    27 - 28 knight
    29 - 30 bishop
    31 queen
    32 king
    */

    // Variables que nos ayudaran con el codigo
    private DAOStats daoStats = new DAOStats();
    private Stat stat = daoStats.getByUserId(LoginController.user.getId());
    private Piece p = null;
    private boolean white = true;
    private boolean end = false;
    private boolean selected = false;
    private boolean validMovement = false;

    /*// Sockets
    private static boolean endConection = false;
    private static int PORT = 9090;
    public static boolean serverTurn = true;
    private static BufferedReader in = null;
    private static PrintWriter out = null;
    private static String messageToSend = null;*/

    // Variables que usaremos para enviar las piezas a los lados
    private Coordinate positionWhitePawn = new Coordinate(0, 7);
    private Coordinate positionWhiteKnight = new Coordinate(1, 7);
    private Coordinate positionWhiteBishop = new Coordinate(2, 7);
    private Coordinate positionWhiteRook = new Coordinate(3, 7);
    private Coordinate positionWhiteQueen = new Coordinate(4, 7);

    private Coordinate positionBlackPawn = new Coordinate(0, 0);
    private Coordinate positionBlackKnight = new Coordinate(1, 0);
    private Coordinate positionBlackBishop = new Coordinate(2, 0);
    private Coordinate positionBlackRook = new Coordinate(3, 0);
    private Coordinate positionBlackQueen = new Coordinate(4, 0);

    AnchorPane[][] pane = new AnchorPane[8][8];
    ImageView[][] image = new ImageView[8][8];

    @FXML
    private GridPane board;

    @FXML
    private GridPane gridPaneWhite;

    @FXML
    private GridPane gridPaneBlack;

    @FXML
    private Text turnText;

    //private ImageView point = new ImageView(new Image(getClass().getResourceAsStream("../images/point.png")));
    // Imagenes de las piezas negras
    private ImageView blackqueenImage = new ImageView(new Image(getClass().getResourceAsStream("../images/blackqueen.png")));
    private ImageView blackkingImage = new ImageView(new Image(getClass().getResourceAsStream("../images/blackking.png")));
    private ImageView blackrookImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackrook.png")));
    private ImageView blackrookImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackrook.png")));
    private ImageView blackbishopImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackbishop.png")));
    private ImageView blackbishopImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackbishop.png")));
    private ImageView blackknightImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackknight.png")));
    private ImageView blackknightImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackknight.png")));
    private ImageView blackpawnImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage3 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage4 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage5 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage6 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage7 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
    private ImageView blackpawnImage8 = new ImageView(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));

    //private ImageView point = new ImageView(new Image(getClass().getResourceAsStream("../images/point.png")));
    // Imagenes de las piezas blancas
    private ImageView whitequeenImage = new ImageView(new Image(getClass().getResourceAsStream("../images/whitequeen.png")));
    private ImageView whitekingImage = new ImageView(new Image(getClass().getResourceAsStream("../images/whiteking.png")));
    private ImageView whiterookImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/whiterook.png")));
    private ImageView whiterookImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/whiterook.png")));
    private ImageView whitebishopImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitebishop.png")));
    private ImageView whitebishopImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitebishop.png")));
    private ImageView whiteknightImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/whiteknight.png")));
    private ImageView whiteknightImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/whiteknight.png")));
    private ImageView whitepawnImage1 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage2 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage3 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage4 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage5 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage6 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage7 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
    private ImageView whitepawnImage8 = new ImageView(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));

    // ArrayList de piezas y de posiciones
    ArrayList<Piece> pieces = new ArrayList<>();
    ArrayList<Coordinate> positions = new ArrayList<Coordinate>();

    /**
     * Funcion que se ejecutara al inicio del fxml
     */
    public void initialize() {
        // Se le suma 1 a las partidas djugadas del usuario
        stat.setGamesPlayed(stat.getGamesPlayed() + 1);

        // Usamos un boolean que va cambiando cada turno para definir el texto de arriba que indica de quien es el turno
        if (white == true) {
            turnText.setText("White's turn");
        } else {
            turnText.setText("Black's turn");
        }

        // GridpaneWhite
        // For para crear el panel donde se guardaran las piezas blancas eliminadas
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 5; j++) {
                image[i][j] = new ImageView();
                int finalJ = j; // Row
                int finalI = i; // Column

                switch (finalJ) {
                    case 0:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/whitepawn.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneWhite.add(image[i][j], i, j);
                        break;

                    case 1:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/whiteknight.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneWhite.add(image[i][j], i, j);
                        break;

                    case 2:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/whitebishop.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneWhite.add(image[i][j], i, j);
                        break;

                    case 3:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/whiterook.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneWhite.add(image[i][j], i, j);
                        break;

                    case 4:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/whiteQueen.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneWhite.add(image[i][j], i, j);
                        break;
                }
            }
        }

        // GridpaneBlack
        // For para crear el panel donde se guardaran las piezas negras eliminadas
        for (int i = 0; i < 8; i++) { ;
            for (int j = 0; j < 5; j++) {
                image[i][j] = new ImageView();
                int finalJ = j; // Row
                int finalI = i; // Column

                switch (finalJ) {
                    case 0:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/blackpawn.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneBlack.add(image[i][j], i, j);
                        break;

                    case 1:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/blackknight.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneBlack.add(image[i][j], i, j);
                        break;

                    case 2:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/blackbishop.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneBlack.add(image[i][j], i, j);
                        break;

                    case 3:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/blackrook.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneBlack.add(image[i][j], i, j);
                        break;

                    case 4:
                        image[i][j].setImage(new Image(getClass().getResourceAsStream("../images/blackqueen.png")));
                        image[i][j].setVisible(false);
                        image[i][j].setScaleX(0.5);
                        image[i][j].setScaleY(0.5);
                        gridPaneBlack.add(image[i][j], i, j);
                        break;
                }
            }
        }

        // Board
        // Creacion del tablero
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                pane[i][j] = new AnchorPane();
                int finalJ = j; // Row
                int finalI = i; // Column

                //pane[i][j].setId("pane" + i + "" + j);

                // Assignem els colors
                if (finalJ % 2 == 0) {
                    if (finalI % 2 == 0) {
                        pane[i][j].setStyle("-fx-background-color :  #ebebd0");
                    } else {
                        pane[i][j].setStyle("-fx-background-color :  #88001b");
                    }
                } else {
                    if (finalI % 2 == 0) {
                        pane[i][j].setStyle("-fx-background-color :  #88001b");
                    } else {
                        pane[i][j].setStyle("-fx-background-color :  #ebebd0");
                    }
                }

                // Le poneemos a los panes de ltablero un onClick
                pane[i][j].setOnMouseClicked(event -> {
                    System.out.println("Cel-la seleccionada: [" + finalJ + ", " + finalI + "]");

                    // Si ya hay una pieza seleccionada
                    if (selected) {
                        // Miramos todas las posiciones validas de la pieza
                        for (Coordinate pos : positions) {
                            AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                            // Se les devuelve su color original
                            if (pos.getRow() % 2 == 0) {
                                if (pos.getColumn() % 2 == 0) {
                                    pane.setStyle("-fx-background-color :  #ebebd0");
                                } else {
                                    pane.setStyle("-fx-background-color :  #88001b");
                                }
                            } else {
                                if (pos.getColumn() % 2 == 0) {
                                    pane.setStyle("-fx-background-color :  #88001b");
                                } else {
                                    pane.setStyle("-fx-background-color :  #ebebd0");
                                }
                            }

                            // Si la posicion es igual al movimiento que quiere hacer el jugador, validMovement = true
                            if (pos.getRow() == finalJ && pos.getColumn() == finalI) {
                                validMovement = true;
                            }
                        }

                        // Si el movimiento es valido
                        if (validMovement) {
                            // Si hay una pieza en la casilla la eliminamos
                            String pieceToEatId = ChessUtils.getImageId(finalJ, finalI, board);
                            if (pieceToEatId != null) {
                                Piece pieceToEat = ChessUtils.getPieceById(Integer.parseInt(pieceToEatId), pieces);
                                board.getChildren().remove(pieceToEat.getImage());

                                // Si la pieza es uno de los reyes la partida termina
                                if (pieceToEat.getPieceType().equals("king") && pieceToEat.getColorPiece().equals("white")) {
                                    try {
                                        stat.setGamesLost(stat.getGamesLost() + 1);
                                        stat.setWinStreak(0);

                                        if (stat.getGamesWon() > 0) {
                                            stat.setWinRate(stat.getGamesWon() * 100 / stat.getGamesPlayed());
                                        }

                                        daoStats.updateStat(stat, LoginController.user.getId());

                                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                        alert.setTitle("Information");
                                        alert.setHeaderText("GAME OVER");

                                        Stage stage = (Stage) turnText.getScene().getWindow();
                                        stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                                        alert.showAndWait();

                                        stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("play.fxml")), 750, 500));
                                        stage.setResizable(false);
                                        stage.centerOnScreen();
                                        stage.show();

                                        //FXMLLoader loader = new FXMLLoader(getClass().getResource("play.fxml"));
                                        /*Scene scene = new Scene(loader.load());
                                        stage.centerOnScreen();
                                        stage.setScene(scene);*/
                                    } catch (IOException ioException) {
                                        ioException.printStackTrace();
                                    }
                                } else if (pieceToEat.getPieceType().equals("king") && pieceToEat.getColorPiece().equals("black")) {
                                    try {
                                        stat.setGamesWon(stat.getGamesWon() + 1);
                                        stat.setWinStreak(stat.getWinStreak() + 1);

                                        if (stat.getGamesWon() > 0) {
                                            stat.setWinRate(stat.getGamesWon() * 100 / stat.getGamesPlayed());
                                        }

                                        daoStats.updateStat(stat, LoginController.user.getId());

                                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                        alert.setTitle("Information");
                                        alert.setHeaderText("GAME OVER");

                                        Stage stage = (Stage) turnText.getScene().getWindow();
                                        stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                                        alert.showAndWait();

                                        stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("play.fxml")), 750, 500));
                                        stage.setResizable(false);
                                        stage.centerOnScreen();
                                        stage.show();

                                        /*FXMLLoader loader = new FXMLLoader(getClass().getResource("play.fxml"));
                                        Scene scene = new Scene(loader.load());
                                        stage.centerOnScreen();
                                        stage.setScene(scene);*/
                                    } catch (IOException ioException) {
                                        ioException.printStackTrace();
                                    }
                                }

                                // Mandamos la pieza eliminada a uno de los lados
                                if (pieceToEat.getColorPiece().equals("white")) {
                                    switch (pieceToEat.getPieceType()){
                                        case "pawn":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneWhite, positionWhitePawn);
                                            break;
                                        case "rook":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneWhite, positionWhiteRook);
                                            break;
                                        case "knight":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneWhite, positionWhiteKnight);
                                            break;
                                        case "bishop":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneWhite, positionWhiteBishop);
                                            break;
                                        case "queen":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneWhite, positionWhiteQueen);
                                            break;
                                    }
                                } else {
                                    switch (pieceToEat.getPieceType()){
                                        case "pawn":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneBlack, positionBlackPawn);
                                            break;
                                        case "rook":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneBlack, positionBlackRook);
                                            break;
                                        case "knight":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneBlack, positionBlackKnight);
                                            break;
                                        case "bishop":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneBlack, positionBlackBishop);
                                            break;
                                        case "queen":
                                            ChessUtils.movePieceToBench(pieceToEat, gridPaneBlack, positionBlackQueen);
                                            break;
                                    }
                                }
                            }

                            p.setRow(finalJ);
                            p.setColumn(finalI);

                            board.setRowIndex(p.getImage(), finalJ);
                            board.setColumnIndex(p.getImage(), finalI);

                            // Coronar peon
                            if (p.getPieceType().equals("pawn")) {

                                //Si la pieza es blanca
                                if (p.getColorPiece().equals("white") && p.getRow() == 0) {
                                    FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                                    Scene scene = null;
                                    try {
                                        scene = new Scene(loader.load(), 600, 200);
                                        Stage stage = new Stage();
                                        stage.setResizable(false);
                                        stage.setScene(scene);
                                        stage.initModality(Modality.APPLICATION_MODAL);
                                        stage.showAndWait();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    // Elegir en que se convierte el peon
                                    if (PawnController.knight){
                                        p.setPieceType("knight");
                                        Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                                        p.getImage().setImage(imageTmp);
                                    } else {
                                        p.setPieceType("queen");
                                        Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                                        p.getImage().setImage(imageTmp);
                                    }
                                }

                                // Si la pieza es negra
                                if (p.getColorPiece().equals("black") && p.getRow() == 7) {
                                    FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                                    Scene scene = null;
                                    try {
                                        scene = new Scene(loader.load(), 600, 200);
                                        Stage stage = new Stage();
                                        stage.setScene(scene);
                                        stage.initModality(Modality.APPLICATION_MODAL);
                                        stage.showAndWait();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    // Elegir en que se convierte el peon
                                    if (PawnController.knight){
                                        p.setPieceType("knight");
                                        Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                                        p.getImage().setImage(imageTmp);
                                    } else {
                                        p.setPieceType("queen");
                                        Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                                        p.getImage().setImage(imageTmp);
                                    }
                                }
                            }

                            // Se resetean las variables
                            positions.clear();
                            selected = false;
                            validMovement = false;

                            if (white) {
                                white = false;
                                turnText.setText("Black's turn");
                            } else {
                                white = true;
                                turnText.setText("White's turn");
                            }
                        }
                    // Si es el primer click, es decir no hay pieza seleccionada
                    } else {
                        // Cogemos el id de la pieza
                        String pieceId = ChessUtils.getImageId(finalJ, finalI, board);
                        if (pieceId != null) {
                            // Cogemos la pieza que tenga ese id
                            Piece piece = ChessUtils.getPieceById(Integer.parseInt(pieceId), pieces);

                            // Si el color de la pieza pulsada y del turno coinciden
                            if (piece.getColorPiece().equals("white") && white || piece.getColorPiece().equals("black") && !white) {
                                p = piece;

                                // Se miran sus posibles movimientos
                                ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                                // Se pintan sus posibles movimientos
                                for (Coordinate pos : positions) {
                                    AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                                    pane.setStyle("-fx-background-color :  #FF607F");
                                }

                                // Selected = true
                                selected = true;
                            } else {
                                System.out.println("No es tu turno");
                            }
                        } else {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Information");
                            alert.setHeaderText("That position is empty");
                            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                            stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                            alert.showAndWait();
                        }
                    }
                });

                // Se añade el panel al tablero
                board.add(pane[i][j], i, j);
            }
        }

        // Coords
        Text num8 = new Text("8");
        num8.setTranslateX(3);
        num8.setTranslateY(-32);
        num8.setFill(Color.valueOf("#88001b"));
        num8.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num8, 0);
        board.setColumnIndex(num8, 0);

        Text num7 = new Text("7");
        num7.setTranslateX(3);
        num7.setTranslateY(-32);
        num7.setFill(Color.valueOf("#ebebd0"));
        num7.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num7, 1);
        board.setColumnIndex(num7, 0);

        Text num6 = new Text("6");
        num6.setTranslateX(3);
        num6.setTranslateY(-32);
        num6.setFill(Color.valueOf("#88001b"));
        num6.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num6, 2);
        board.setColumnIndex(num6, 0);

        Text num5 = new Text("5");
        num5.setTranslateX(3);
        num5.setTranslateY(-32);
        num5.setFill(Color.valueOf("#ebebd0"));
        num5.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num5, 3);
        board.setColumnIndex(num5, 0);

        Text num4 = new Text("4");
        num4.setTranslateX(3);
        num4.setTranslateY(-32);
        num4.setFill(Color.valueOf("#88001b"));
        num4.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num4, 4);
        board.setColumnIndex(num4, 0);

        Text num3 = new Text("3");
        num3.setTranslateX(3);
        num3.setTranslateY(-32);
        num3.setFill(Color.valueOf("#ebebd0"));
        num3.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num3, 5);
        board.setColumnIndex(num3, 0);

        Text num2 = new Text("2");
        num2.setTranslateX(3);
        num2.setTranslateY(-32);
        num2.setFill(Color.valueOf("#88001b"));
        num2.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num2, 6);
        board.setColumnIndex(num2, 0);

        Text num1 = new Text("1");
        num1.setTranslateX(3);
        num1.setTranslateY(-32);
        num1.setFill(Color.valueOf("#ebebd0"));
        num1.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(num1, 7);
        board.setColumnIndex(num1, 0);

        Text a = new Text("a");
        a.setTranslateX(70);
        a.setTranslateY(32);
        a.setFill(Color.valueOf("#ebebd0"));
        a.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(a, 7);
        board.setColumnIndex(a, 0);

        Text b = new Text("b");
        b.setTranslateX(70);
        b.setTranslateY(32);
        b.setFill(Color.valueOf("#88001b"));
        b.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(b, 7);
        board.setColumnIndex(b, 1);

        Text c = new Text("c");
        c.setTranslateX(70);
        c.setTranslateY(32);
        c.setFill(Color.valueOf("#ebebd0"));
        c.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(c, 7);
        board.setColumnIndex(c, 2);

        Text d = new Text("d");
        d.setTranslateX(70);
        d.setTranslateY(32);
        d.setFill(Color.valueOf("#88001b"));
        d.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(d, 7);
        board.setColumnIndex(d, 3);

        Text e = new Text("e");
        e.setTranslateX(70);
        e.setTranslateY(32);
        e.setFill(Color.valueOf("#ebebd0"));
        e.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(e, 7);
        board.setColumnIndex(e, 4);

        Text f = new Text("f");
        f.setTranslateX(73);
        f.setTranslateY(32);
        f.setFill(Color.valueOf("#88001b"));
        f.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(f, 7);
        board.setColumnIndex(f, 5);

        Text g = new Text("g");
        g.setTranslateX(70);
        g.setTranslateY(30);
        g.setFill(Color.valueOf("#ebebd0"));
        g.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(g, 7);
        board.setColumnIndex(g, 6);

        Text h = new Text("h");
        h.setTranslateX(70);
        h.setTranslateY(32);
        h.setFill(Color.valueOf("#88001b"));
        h.setFont(Font.font("System", FontWeight.BOLD, 18));
        board.setRowIndex(h, 7);
        board.setColumnIndex(h, 7);

        // White pieces
        // Se le asigna el id a la imagen, la posicion, y se crea una pieza con esa imagen
        whitepawnImage1.setId("1");
        board.setRowIndex(whitepawnImage1, 6);
        board.setColumnIndex(whitepawnImage1, 0);
        Piece whitepawn1 = new Piece(1, "pawn", "white", whitepawnImage1, 6, 0);
        // Cuando le des click a la pieza
        whitepawnImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza si es una pieza negra
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                // Se mira si el movimiento es valido
                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn1.getRow() && pos.getColumn() == whitepawn1.getColumn()) {
                        validMovement = true;
                    }
                }

                // Si es un movimiento valido
                if (validMovement) {
                    // Se elimina del tablero la imagen de la pieza eliminada
                    board.getChildren().remove(whitepawn1.getImage());

                    // Se mueve la pieza
                    p.setRow(whitepawn1.getRow());
                    p.setColumn(whitepawn1.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn1.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn1.getColumn());

                    // Se mueve la pieza a uno de los lados
                    ChessUtils.movePieceToBench(whitepawn1, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;

                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            // Si es el prrimer click
            } else {
                // Seleccionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }

                    positions.clear();

                    p = whitepawn1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    // Se miran sus posibles movimientos
                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;

                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    // Se pintan sus posibles movimientos
                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    // Se selecciona la pieza
                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(p.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage2.setId("2");
        board.setRowIndex(whitepawnImage2, 6);
        board.setColumnIndex(whitepawnImage2, 1);
        Piece whitepawn2 = new Piece(2, "pawn", "white", whitepawnImage2, 6, 1);
        whitepawnImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn2.getRow() && pos.getColumn() == whitepawn2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn2.getImage());

                    p.setRow(whitepawn2.getRow());
                    p.setColumn(whitepawn2.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn2.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn2.getColumn());

                    ChessUtils.movePieceToBench(whitepawn2, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage3.setId("3");
        board.setRowIndex(whitepawnImage3, 6);
        board.setColumnIndex(whitepawnImage3, 2);
        Piece whitepawn3 = new Piece(3, "pawn", "white", whitepawnImage3, 6, 2);
        whitepawnImage3.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn3.getRow() && pos.getColumn() == whitepawn3.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn3.getImage());

                    p.setRow(whitepawn3.getRow());
                    p.setColumn(whitepawn3.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn3.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn3.getColumn());

                    ChessUtils.movePieceToBench(whitepawn3, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn3;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage4.setId("4");
        board.setRowIndex(whitepawnImage4, 6);
        board.setColumnIndex(whitepawnImage4, 3);
        Piece whitepawn4 = new Piece(4, "pawn", "white", whitepawnImage4, 6, 3);
        whitepawnImage4.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn4.getRow() && pos.getColumn() == whitepawn4.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn4.getImage());

                    p.setRow(whitepawn4.getRow());
                    p.setColumn(whitepawn4.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn4.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn4.getColumn());

                    ChessUtils.movePieceToBench(whitepawn4, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn4;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage5.setId("5");
        board.setRowIndex(whitepawnImage5, 6);
        board.setColumnIndex(whitepawnImage5, 4);
        Piece whitepawn5 = new Piece(5, "pawn", "white", whitepawnImage5, 6, 4);
        whitepawnImage5.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn5.getRow() && pos.getColumn() == whitepawn5.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn5.getImage());

                    p.setRow(whitepawn5.getRow());
                    p.setColumn(whitepawn5.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn5.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn5.getColumn());

                    ChessUtils.movePieceToBench(whitepawn5, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn5;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage6.setId("6");
        board.setRowIndex(whitepawnImage6, 6);
        board.setColumnIndex(whitepawnImage6, 5);
        Piece whitepawn6 = new Piece(6, "pawn", "white", whitepawnImage6, 6, 5);
        whitepawnImage6.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn6.getRow() && pos.getColumn() == whitepawn6.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn6.getImage());

                    p.setRow(whitepawn6.getRow());
                    p.setColumn(whitepawn6.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn6.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn6.getColumn());

                    ChessUtils.movePieceToBench(whitepawn6, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn6;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage7.setId("7");
        board.setRowIndex(whitepawnImage7, 6);
        board.setColumnIndex(whitepawnImage7, 6);
        Piece whitepawn7 = new Piece(7, "pawn", "white", whitepawnImage7, 6, 6);
        whitepawnImage7.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn7.getRow() && pos.getColumn() == whitepawn7.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn7.getImage());

                    p.setRow(whitepawn7.getRow());
                    p.setColumn(whitepawn7.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn7.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn7.getColumn());

                    ChessUtils.movePieceToBench(whitepawn7, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn7;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitepawnImage8.setId("8");
        board.setRowIndex(whitepawnImage8, 6);
        board.setColumnIndex(whitepawnImage8, 7);
        Piece whitepawn8 = new Piece(8, "pawn", "white", whitepawnImage8, 6, 7);
        whitepawnImage8.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitepawn8.getRow() && pos.getColumn() == whitepawn8.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitepawn8.getImage());

                    p.setRow(whitepawn8.getRow());
                    p.setColumn(whitepawn8.getColumn());

                    board.setRowIndex(p.getImage(), whitepawn8.getRow());
                    board.setColumnIndex(p.getImage(), whitepawn8.getColumn());

                    ChessUtils.movePieceToBench(whitepawn8, gridPaneWhite, positionWhitePawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitepawn8;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whiterookImage1.setId("9");
        board.setRowIndex(whiterookImage1, 7);
        board.setColumnIndex(whiterookImage1, 0);
        Piece whiterook1 = new Piece(9, "rook", "white", whiterookImage1, 7, 0);
        whiterookImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whiterook1.getRow() && pos.getColumn() == whiterook1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whiterook1.getImage());

                    p.setRow(whiterook1.getRow());
                    p.setColumn(whiterook1.getColumn());

                    board.setRowIndex(p.getImage(), whiterook1.getRow());
                    board.setColumnIndex(p.getImage(), whiterook1.getColumn());

                    ChessUtils.movePieceToBench(whiterook1, gridPaneWhite, positionWhiteRook);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whiterook1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);


                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whiterookImage2.setId("10");
        // Nos comemos la pieza
        board.setRowIndex(whiterookImage2, 7);
        board.setColumnIndex(whiterookImage2, 7);
        Piece whiterook2 = new Piece(10, "rook", "white", whiterookImage2, 7, 7);
        whiterookImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whiterook2.getRow() && pos.getColumn() == whiterook2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whiterook2.getImage());

                    p.setRow(whiterook2.getRow());
                    p.setColumn(whiterook2.getColumn());

                    board.setRowIndex(p.getImage(), whiterook2.getRow());
                    board.setColumnIndex(p.getImage(), whiterook2.getColumn());

                    ChessUtils.movePieceToBench(whiterook2, gridPaneWhite, positionWhiteRook);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whiterook2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whiteknightImage1.setId("11");
        board.setRowIndex(whiteknightImage1, 7);
        board.setColumnIndex(whiteknightImage1, 1);
        Piece whiteknight1 = new Piece(11, "knight", "white", whiteknightImage1, 7, 1);
        whiteknightImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whiteknight1.getRow() && pos.getColumn() == whiteknight1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whiteknight1.getImage());

                    p.setRow(whiteknight1.getRow());
                    p.setColumn(whiteknight1.getColumn());

                    board.setRowIndex(p.getImage(), whiteknight1.getRow());
                    board.setColumnIndex(p.getImage(), whiteknight1.getColumn());

                    ChessUtils.movePieceToBench(whiteknight1, gridPaneWhite, positionWhiteKnight);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whiteknight1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whiteknightImage2.setId("12");
        board.setRowIndex(whiteknightImage2, 7);
        board.setColumnIndex(whiteknightImage2, 6);
        Piece whiteknight2 = new Piece(12, "knight", "white", whiteknightImage2, 7, 6);
        whiteknightImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whiteknight2.getRow() && pos.getColumn() == whiteknight2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whiteknight2.getImage());

                    p.setRow(whiteknight2.getRow());
                    p.setColumn(whiteknight2.getColumn());

                    board.setRowIndex(p.getImage(), whiteknight2.getRow());
                    board.setColumnIndex(p.getImage(), whiteknight2.getColumn());

                    ChessUtils.movePieceToBench(whiteknight2, gridPaneWhite, positionWhiteKnight);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whiteknight2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitebishopImage1.setId("13");
        board.setRowIndex(whitebishopImage1, 7);
        board.setColumnIndex(whitebishopImage1, 2);
        Piece whitebishop1 = new Piece(13, "bishop", "white", whitebishopImage1, 7, 2);
        whitebishopImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitebishop1.getRow() && pos.getColumn() == whitebishop1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitebishop1.getImage());

                    p.setRow(whitebishop1.getRow());
                    p.setColumn(whitebishop1.getColumn());

                    board.setRowIndex(p.getImage(), whitebishop1.getRow());
                    board.setColumnIndex(p.getImage(), whitebishop1.getColumn());

                    ChessUtils.movePieceToBench(whitebishop1, gridPaneWhite, positionWhiteBishop);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitebishop1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitebishopImage2.setId("14");
        board.setRowIndex(whitebishopImage2, 7);
        board.setColumnIndex(whitebishopImage2, 5);
        Piece whitebishop2 = new Piece(14, "bishop", "white", whitebishopImage2, 7, 5);
        whitebishopImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitebishop2.getRow() && pos.getColumn() == whitebishop2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitebishop2.getImage());

                    p.setRow(whitebishop2.getRow());
                    p.setColumn(whitebishop2.getColumn());

                    board.setRowIndex(p.getImage(), whitebishop2.getRow());
                    board.setColumnIndex(p.getImage(), whitebishop2.getColumn());

                    ChessUtils.movePieceToBench(whitebishop2, gridPaneWhite, positionWhiteBishop);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitebishop2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitequeenImage.setId("15");
        board.setRowIndex(whitequeenImage, 7);
        board.setColumnIndex(whitequeenImage, 3);
        Piece whitequeen = new Piece(15, "queen", "white", whitequeenImage, 7, 3);
        whitequeenImage.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whitequeen.getRow() && pos.getColumn() == whitequeen.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whitequeen.getImage());

                    p.setRow(whitequeen.getRow());
                    p.setColumn(whitequeen.getColumn());

                    board.setRowIndex(p.getImage(), whitequeen.getRow());
                    board.setColumnIndex(p.getImage(), whitequeen.getColumn());

                    ChessUtils.movePieceToBench(whitequeen, gridPaneWhite, positionWhiteQueen);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 7) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/blackqueen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whitequeen;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        whitekingImage.setId("16");
        board.setRowIndex(whitekingImage, 7);
        board.setColumnIndex(whitekingImage, 4);
        Piece whiteking = new Piece(16, "king", "white", whitekingImage, 7, 4);
        whitekingImage.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && !white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == whiteking.getRow() && pos.getColumn() == whiteking.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(whiteking.getImage());
                    end = true;


                    p.setRow(whiteking.getRow());
                    p.setColumn(whiteking.getColumn());

                    board.setRowIndex(p.getImage(), whiteking.getRow());
                    board.setColumnIndex(p.getImage(), whiteking.getColumn());

                    if (end) {
                        try {
                            stat.setGamesLost(stat.getGamesLost() + 1);
                            stat.setWinStreak(0);

                            if (stat.getGamesWon() > 0) {
                                stat.setWinRate(stat.getGamesWon() * 100 / stat.getGamesPlayed());
                            }

                            daoStats.updateStat(stat, LoginController.user.getId());

                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Information");
                            alert.setHeaderText("GAME OVER");

                            Stage stage = (Stage) turnText.getScene().getWindow();
                            stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                            alert.showAndWait();

                            stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("play.fxml")), 750, 500));
                            stage.setResizable(false);
                            stage.centerOnScreen();
                            stage.show();

                            /*FXMLLoader loader = new FXMLLoader(getClass().getResource("play.fxml"));
                            Scene scene = new Scene(loader.load());
                            stage.setScene(scene);*/
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = whiteking;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        pieces.add(whitepawn1);
        pieces.add(whitepawn2);
        pieces.add(whitepawn3);
        pieces.add(whitepawn4);
        pieces.add(whitepawn5);
        pieces.add(whitepawn6);
        pieces.add(whitepawn7);
        pieces.add(whitepawn8);
        pieces.add(whiterook1);
        pieces.add(whiterook2);
        pieces.add(whiteknight1);
        pieces.add(whiteknight2);
        pieces.add(whitebishop1);
        pieces.add(whitebishop2);
        pieces.add(whitequeen);
        pieces.add(whiteking);

        // Black pieces
        blackpawnImage1.setId("17");
        board.setRowIndex(blackpawnImage1, 1);
        board.setColumnIndex(blackpawnImage1, 0);
        Piece blackpawn1 = new Piece(17, "pawn", "black", blackpawnImage1, 1, 0);
        blackpawnImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn1.getRow() && pos.getColumn() == blackpawn1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn1.getImage());

                    p.setRow(blackpawn1.getRow());
                    p.setColumn(blackpawn1.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn1.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn1.getColumn());

                    ChessUtils.movePieceToBench(blackpawn1, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackpawnImage2.setId("18");
        board.setRowIndex(blackpawnImage2, 1);
        board.setColumnIndex(blackpawnImage2, 1);
        Piece blackpawn2 = new Piece(18, "pawn", "black", blackpawnImage2, 1, 1);
        blackpawnImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn2.getRow() && pos.getColumn() == blackpawn2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn2.getImage());

                    p.setRow(blackpawn2.getRow());
                    p.setColumn(blackpawn2.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn2.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn2.getColumn());

                    ChessUtils.movePieceToBench(blackpawn2, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackpawnImage3.setId("19");
        board.setRowIndex(blackpawnImage3, 1);
        board.setColumnIndex(blackpawnImage3, 2);
        Piece blackpawn3 = new Piece(19, "pawn", "black", blackpawnImage3, 1, 2);
        blackpawnImage3.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn3.getRow() && pos.getColumn() == blackpawn3.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn3.getImage());

                    p.setRow(blackpawn3.getRow());
                    p.setColumn(blackpawn3.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn3.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn3.getColumn());

                    ChessUtils.movePieceToBench(blackpawn3, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn3;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }

        });

        blackpawnImage4.setId("20");
        board.setRowIndex(blackpawnImage4, 1);
        board.setColumnIndex(blackpawnImage4, 3);
        Piece blackpawn4 = new Piece(20, "pawn", "black", blackpawnImage4, 1, 3);
        blackpawnImage4.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn4.getRow() && pos.getColumn() == blackpawn4.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn4.getImage());

                    p.setRow(blackpawn4.getRow());
                    p.setColumn(blackpawn4.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn4.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn4.getColumn());

                    ChessUtils.movePieceToBench(blackpawn4, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn4;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackpawnImage5.setId("21");
        board.setRowIndex(blackpawnImage5, 1);
        board.setColumnIndex(blackpawnImage5, 4);
        Piece blackpawn5 = new Piece(21, "pawn", "black", blackpawnImage5, 1, 4);
        blackpawnImage5.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn5.getRow() && pos.getColumn() == blackpawn5.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn5.getImage());

                    p.setRow(blackpawn5.getRow());
                    p.setColumn(blackpawn5.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn5.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn5.getColumn());

                    ChessUtils.movePieceToBench(blackpawn5, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn5;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackpawnImage6.setId("22");
        board.setRowIndex(blackpawnImage6, 1);
        board.setColumnIndex(blackpawnImage6, 5);
        Piece blackpawn6 = new Piece(22, "pawn", "black", blackpawnImage6, 1, 5);
        blackpawnImage6.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn6.getRow() && pos.getColumn() == blackpawn6.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn6.getImage());

                    p.setRow(blackpawn6.getRow());
                    p.setColumn(blackpawn6.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn6.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn6.getColumn());

                    ChessUtils.movePieceToBench(blackpawn6, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn6;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackpawnImage7.setId("23");
        board.setRowIndex(blackpawnImage7, 1);
        board.setColumnIndex(blackpawnImage7, 6);
        Piece blackpawn7 = new Piece(23, "pawn", "black", blackpawnImage7, 1, 6);
        blackpawnImage7.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn7.getRow() && pos.getColumn() == blackpawn7.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn7.getImage());

                    p.setRow(blackpawn7.getRow());
                    p.setColumn(blackpawn7.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn7.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn7.getColumn());

                    ChessUtils.movePieceToBench(blackpawn7, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn7;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackpawnImage8.setId("24");
        board.setRowIndex(blackpawnImage8, 1);
        board.setColumnIndex(blackpawnImage8, 7);
        Piece blackpawn8 = new Piece(24, "pawn", "black", blackpawnImage8, 1, 7);
        blackpawnImage8.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackpawn8.getRow() && pos.getColumn() == blackpawn8.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackpawn8.getImage());

                    p.setRow(blackpawn8.getRow());
                    p.setColumn(blackpawn8.getColumn());

                    board.setRowIndex(p.getImage(), blackpawn8.getRow());
                    board.setColumnIndex(p.getImage(), blackpawn8.getColumn());

                    ChessUtils.movePieceToBench(blackpawn8, gridPaneBlack, positionBlackPawn);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackpawn8;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackrookImage1.setId("25");
        board.setRowIndex(blackrookImage1, 0);
        board.setColumnIndex(blackrookImage1, 0);
        Piece blackrook1 = new Piece(25, "rook", "black", blackrookImage1, 0, 0);
        blackrookImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackrook1.getRow() && pos.getColumn() == blackrook1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackrook1.getImage());

                    p.setRow(blackrook1.getRow());
                    p.setColumn(blackrook1.getColumn());

                    board.setRowIndex(p.getImage(), blackrook1.getRow());
                    board.setColumnIndex(p.getImage(), blackrook1.getColumn());

                    ChessUtils.movePieceToBench(blackrook1, gridPaneBlack, positionBlackRook);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackrook1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackrookImage2.setId("26");
        board.setRowIndex(blackrookImage2, 0);
        board.setColumnIndex(blackrookImage2, 7);
        Piece blackrook2 = new Piece(26, "rook", "black", blackrookImage2, 0, 7);
        blackrookImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackrook2.getRow() && pos.getColumn() == blackrook2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackrook2.getImage());

                    p.setRow(blackrook2.getRow());
                    p.setColumn(blackrook2.getColumn());

                    board.setRowIndex(p.getImage(), blackrook2.getRow());
                    board.setColumnIndex(p.getImage(), blackrook2.getColumn());

                    ChessUtils.movePieceToBench(blackrook2, gridPaneBlack, positionBlackRook);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackrook2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackknightImage1.setId("27");
        board.setRowIndex(blackknightImage1, 0);
        board.setColumnIndex(blackknightImage1, 1);
        Piece blackknight1 = new Piece(27, "knight", "black", blackknightImage1, 0, 1);
        blackknightImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackknight1.getRow() && pos.getColumn() == blackknight1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackknight1.getImage());

                    p.setRow(blackknight1.getRow());
                    p.setColumn(blackknight1.getColumn());

                    board.setRowIndex(p.getImage(), blackknight1.getRow());
                    board.setColumnIndex(p.getImage(), blackknight1.getColumn());

                    ChessUtils.movePieceToBench(blackknight1, gridPaneBlack, positionBlackKnight);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackknight1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackknightImage2.setId("28");
        board.setRowIndex(blackknightImage2, 0);
        board.setColumnIndex(blackknightImage2, 6);
        Piece blackknight2 = new Piece(28, "knight", "black", blackknightImage2, 0, 6);
        blackknightImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackknight2.getRow() && pos.getColumn() == blackknight2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackknight2.getImage());

                    p.setRow(blackknight2.getRow());
                    p.setColumn(blackknight2.getColumn());

                    board.setRowIndex(p.getImage(), blackknight2.getRow());
                    board.setColumnIndex(p.getImage(), blackknight2.getColumn());

                    ChessUtils.movePieceToBench(blackknight2, gridPaneBlack, positionBlackKnight);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackknight2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackbishopImage1.setId("29");
        board.setRowIndex(blackbishopImage1, 0);
        board.setColumnIndex(blackbishopImage1, 2);
        Piece blackbishop1 = new Piece(29, "bishop", "black", blackbishopImage1, 0, 2);
        blackbishopImage1.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackbishop1.getRow() && pos.getColumn() == blackbishop1.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackbishop1.getImage());

                    p.setRow(blackbishop1.getRow());
                    p.setColumn(blackbishop1.getColumn());

                    board.setRowIndex(p.getImage(), blackbishop1.getRow());
                    board.setColumnIndex(p.getImage(), blackbishop1.getColumn());

                    ChessUtils.movePieceToBench(blackbishop1, gridPaneBlack, positionBlackBishop);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackbishop1;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackbishopImage2.setId("30");
        board.setRowIndex(blackbishopImage2, 0);
        board.setColumnIndex(blackbishopImage2, 5);
        Piece blackbishop2 = new Piece(30, "bishop", "black", blackbishopImage2, 0, 5);
        blackbishopImage2.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackbishop2.getRow() && pos.getColumn() == blackbishop2.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackbishop2.getImage());

                    p.setRow(blackbishop2.getRow());
                    p.setColumn(blackbishop2.getColumn());

                    board.setRowIndex(p.getImage(), blackbishop2.getRow());
                    board.setColumnIndex(p.getImage(), blackbishop2.getColumn());

                    ChessUtils.movePieceToBench(blackbishop2, gridPaneBlack, positionBlackBishop);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackbishop2;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackqueenImage.setId("31");
        board.setRowIndex(blackqueenImage, 0);
        board.setColumnIndex(blackqueenImage, 3);
        Piece blackqueen = new Piece(31, "queen", "black", blackqueenImage, 0, 3);
        blackqueenImage.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackqueen.getRow() && pos.getColumn() == blackqueen.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackqueen.getImage());

                    p.setRow(blackqueen.getRow());
                    p.setColumn(blackqueen.getColumn());

                    board.setRowIndex(p.getImage(), blackqueen.getRow());
                    board.setColumnIndex(p.getImage(), blackqueen.getColumn());

                    ChessUtils.movePieceToBench(blackqueen, gridPaneBlack, positionBlackQueen);

                    // Coronar peon
                    if (p.getPieceType().equals("pawn") && p.getRow() == 0) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("pawn.fxml"));
                        Scene scene = null;
                        try {
                            scene = new Scene(loader.load(), 600, 200);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.showAndWait();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                        // Elegir en que se convierte el peon
                        if (PawnController.knight){
                            p.setPieceType("knight");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whiteknight.png"));
                            p.getImage().setImage(imageTmp);
                        } else {
                            p.setPieceType("queen");
                            Image imageTmp = new Image(getClass().getResourceAsStream("../images/whitequeen.png"));
                            p.getImage().setImage(imageTmp);
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = false;
                    turnText.setText("Black's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackqueen;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        blackkingImage.setId("32");
        board.setRowIndex(blackkingImage, 0);
        board.setColumnIndex(blackkingImage, 4);
        Piece blackking = new Piece(32, "king", "black", blackkingImage, 0, 4);
        blackkingImage.setOnMouseClicked(event -> {
            // Nos comemos la pieza
            if (selected && white) {
                ChessUtils.recolorPane(positions, board);

                for (Coordinate pos : positions) {
                    if (pos.getRow() == blackking.getRow() && pos.getColumn() == blackking.getColumn()) {
                        validMovement = true;
                    }
                }

                if (validMovement) {
                    board.getChildren().remove(blackking.getImage());
                    end = true;

                    p.setRow(blackking.getRow());
                    p.setColumn(blackking.getColumn());

                    board.setRowIndex(p.getImage(), blackking.getRow());
                    board.setColumnIndex(p.getImage(), blackking.getColumn());

                    if (end) {
                        try {
                            stat.setGamesWon(stat.getGamesWon() + 1);
                            stat.setWinStreak(stat.getWinStreak() + 1);

                            if (stat.getGamesWon() > 0) {
                                stat.setWinRate(stat.getGamesWon() * 100 / stat.getGamesPlayed());
                            }

                            daoStats.updateStat(stat, LoginController.user.getId());

                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Information");
                            alert.setHeaderText("GAME OVER");
                            Stage stage = (Stage) turnText.getScene().getWindow();
                            stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                            alert.showAndWait();

                            stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("play.fxml")), 750, 500));
                            stage.setResizable(false);
                            stage.centerOnScreen();
                            stage.show();

                            /*FXMLLoader loader = new FXMLLoader(getClass().getResource("play.fxml"));
                            Scene scene = new Scene(loader.load());
                            stage.setScene(scene);*/
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }

                    selected = false;
                    validMovement = false;
                    white = true;
                    turnText.setText("White's turn");
                }
            } else {
                // Selecionas la pieza
                if (!white) {
                    if (selected) {
                        ChessUtils.recolorPane(positions, board);
                    }
                    positions.clear();

                    p = blackking;

                    System.out.println("Cel-la seleccionada: [" + p.getRow() + ", " + p.getColumn() + "]");

                    ChessUtils.checkMovements(p, p.getRow(), p.getColumn(), positions, board, pieces);

                    for (Coordinate pos : positions) {
                        AnchorPane pane = (AnchorPane) ChessUtils.getNodeByRowColumn(pos.getRow(), pos.getColumn(), board);
                        pane.setStyle("-fx-background-color :  #FF607F");
                    }

                    selected = true;
                } else {
                    System.out.println("That piece is not yours");
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("That piece is not yours");
                    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage.getIcons().add(new Image(this.getClass().getResource("../images/logo.png").toString()));

                    alert.showAndWait();
                }
            }
        });

        pieces.add(blackpawn1);
        pieces.add(blackpawn2);
        pieces.add(blackpawn3);
        pieces.add(blackpawn4);
        pieces.add(blackpawn5);
        pieces.add(blackpawn6);
        pieces.add(blackpawn7);
        pieces.add(blackpawn8);
        pieces.add(blackrook1);
        pieces.add(blackrook2);
        pieces.add(blackknight1);
        pieces.add(blackknight2);
        pieces.add(blackbishop1);
        pieces.add(blackbishop2);
        pieces.add(blackqueen);
        pieces.add(blackking);

        board.getChildren().addAll(whitepawn1.getImage(), whitepawn2.getImage(), whitepawn3.getImage(), whitepawn4.getImage(), whitepawn5.getImage(), whitepawn6.getImage(), whitepawn7.getImage(),
                whitepawn8.getImage(), whiterook1.getImage(), whiterook2.getImage(), whiteknight1.getImage(), whiteknight2.getImage(), whitebishop1.getImage(), whitebishop2.getImage(),
                whitequeen.getImage(), whiteking.getImage(),
                blackpawn1.getImage(), blackpawn2.getImage(), blackpawn3.getImage(), blackpawn4.getImage(), blackpawn5.getImage(), blackpawn6.getImage(), blackpawn7.getImage(),
                blackpawn8.getImage(), blackrook1.getImage(), blackrook2.getImage(), blackknight1.getImage(), blackknight2.getImage(), blackbishop1.getImage(), blackbishop2.getImage(),
                blackqueen.getImage(), blackking.getImage(),
                num8, num7, num6, num5, num4, num3, num2, num1,
                a, b, c, d, e, f, g, h);

        //Server.escoltar();
    }

    /*public void listen(ActionEvent actionEvent) {
        ServerSocket serverSocket = null;
        Socket clientSocket = null;

        try {
            serverSocket = new ServerSocket(PORT);

            while (!endConection) {
                System.out.println("Esperant Player2...");
                clientSocket = serverSocket.accept();

                procesComunication(clientSocket);

                Server.closeSocket(clientSocket);

                System.out.println("Player2 s'ha desconectat, esperant un altre Player2...");
            }

            if ((serverSocket != null) && (!serverSocket.isClosed())) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    /*public void procesComunication(Socket clientSocket) {
        Scanner sc = new Scanner(System.in);
        String missatgeDelClient = null;
        boolean endComunication = false;

        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

            while (!endComunication) {
                if (serverTurn) {

                    /*while (messageToSend == null){

                    }

                    out.println(messageToSend);
                    out.flush();
                    messageToSend = null;

                    serverTurn = false;
                } else {
                    missatgeDelClient = in.readLine();

                    System.out.println("Missatge del client: " + missatgeDelClient);

                    while(messageToSend == null){

                    }
                    out.println(messageToSend);
                    out.flush();
                    messageToSend = null;

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }*/
}