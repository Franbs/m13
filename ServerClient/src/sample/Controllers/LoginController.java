package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DAO.DAOUser;
import sample.clases.User;

import javax.swing.*;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class LoginController {
    public static User user;

    // Botones
    @FXML
    private Button btSignUp;

    @FXML
    private Button btLogIn;

    // Textos
    @FXML
    private TextField username;

    @FXML
    private TextField password;

    /**
     * Funcion de intent al sign up
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickSignUp(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("signup.fxml"));
        Stage stage = (Stage) btSignUp.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }

    /**
     * Funcion de intent al play despues de completar el log in
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickLogIn(ActionEvent actionEvent) throws IOException {
        try {
            // Se coge un usuario con el texto introducido
            DAOUser daoUser = new DAOUser();
            user = daoUser.getByUsername(username.getText());

            String value = password.getText();

            // Se encripta la contraseña
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(value.getBytes("utf8"));
            String sha1 = String.format("%040x", new BigInteger(1, digest.digest()));

            // Si la contraseña y el email o el username estan en la db
            if ((username.getText().equals(user.getUsername()) || username.getText().equals(user.getEmail())) && sha1.equals(user.getPassword())) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("play.fxml"));
                Stage stage = (Stage) btLogIn.getScene().getWindow();
                Scene scene = new Scene(loader.load());
                stage.setScene(scene);
            // Si el usuario no existe o los datos son incorrectos sale una alerta
            } else {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                errorAlert.setHeaderText("Error");
                errorAlert.setContentText("That user does not exist");
                errorAlert.showAndWait();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}