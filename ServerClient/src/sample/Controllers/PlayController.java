package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sample.DAO.DAOUser;

import java.io.IOException;

/**
 * @author Francesc Banzo
 * @author David Ortega
 */
public class PlayController {
    @FXML
    private Text username;

    @FXML
    private Button btLogOut;

    @FXML
    private Button btPlay;

    @FXML
    private Button btStats;

    public void initialize() {
        username.setText(LoginController.user.getUsername());
    }

    /**
     * Salir de la cuenta, vuelve al log in
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickLogOut(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
        Stage stage = (Stage) btLogOut.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }

    /**
     * Intent al tablero
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickPlay(ActionEvent actionEvent) throws IOException {
        //Parent root = FXMLLoader.load(getClass().getResource("chess.fxml"));
        Stage stage = (Stage) btPlay.getScene().getWindow();
        stage.setTitle("ChessFD");
        stage.setScene(new Scene(FXMLLoader.load(getClass().getResource("chess.fxml")), 1500, 800));
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();

        /*FXMLLoader loader = new FXMLLoader(getClass().getResource("chess.fxml"));
        Stage stage = (Stage) btPlay.getScene().getWindow();
        Scene scene = new Scene(loader.load(), 1500, 800);
        stage.setResizable(false);
        stage.setScene(scene);*/
    }

    /**
     * Intent a la pantalla de las stats
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void clickStats(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("stats.fxml"));
        Stage stage = (Stage) btStats.getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
    }
}